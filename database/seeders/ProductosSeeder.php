<?php

namespace Database\Seeders;

use App\Models\Productos;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class ProductosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Productos::Create([
            'descripcion' => 'Producto 1',
            'precio_impuesto' => '125.43',
            'impuesto' => '5'
        ]);

        Productos::Create([
            'descripcion' => 'Producto 2',
            'precio_impuesto' => '45.65',
            'impuesto' => '15'
        ]);

        Productos::Create([
            'descripcion' => 'Producto 3',
            'precio_impuesto' => '39.73',
            'impuesto' => '12'
        ]);

        Productos::Create([
            'descripcion' => 'Producto 4',
            'precio_impuesto' => '250',
            'impuesto' => '8'
        ]);

        Productos::Create([
            'descripcion' => 'Producto 5',
            'precio_impuesto' => '59.35',
            'impuesto' => '10'
        ]);
    }
}
