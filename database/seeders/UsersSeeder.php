<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::Create([
            'name' => 'Administrador',
            'email' => 'admin@admin.com',
            'password' => bcrypt('12345678')
        ])->assignRole('administrador');

        User::Create([
            'name' => 'Usuario',
            'email' => 'usuario@usuario.com',
            'password' => bcrypt('12345678')
        ]);
    }
}
