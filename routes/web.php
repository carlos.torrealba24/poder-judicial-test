<?php

use App\Http\Controllers\ComprasController;
use App\Http\Controllers\FacturasController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductosController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\ProductosController::class, 'index'])->name('home');


// rutas de productos
Route::get('/productos/index', [ProductosController::class, 'index'])->name('productos.index')->middleware('auth')->middleware('auth');
Route::get('/productos/create', [ProductosController::class, 'create'])->name('productos.create')->middleware('auth');
Route::get('/productos/edit/{prod}', [ProductosController::class, 'edit'])->name('productos.edit')->middleware('auth');
Route::put('/productos/actualizar/{prod}', [ProductosController::class, 'actualizar'])->name('productos.actualizar')->middleware('auth');
Route::delete('/productos/delete/{prod}', [ProductosController::class, 'delete'])->name('productos.delete')->middleware('auth');
Route::post('/productos/register', [ProductosController::class, 'register'])->name('productos.register')->middleware('auth');


// rutas de compras

Route::get('/compras/index', [ComprasController::class, 'index'])->name('compras.index')->middleware('auth');
Route::get('/compras/register/{prod}', [ComprasController::class, 'register'])->name('compras.register')->middleware('auth');

// rutas de facturas
Route::get('/facturas/index', [FacturasController::class, 'index'])->name('facturas.index')->middleware('auth');
Route::get('/facturas-pendientes/index', [FacturasController::class, 'indexPendientes'])->name('facturas.indexPen')->middleware('auth');
Route::get('/facturas-individual/{comp}', [FacturasController::class, 'facturarOne'])->name('facturas.factura_one')->middleware('auth');

