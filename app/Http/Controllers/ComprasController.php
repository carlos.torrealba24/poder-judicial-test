<?php

namespace App\Http\Controllers;

use App\Models\Compras;
use App\Models\Productos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ComprasController extends Controller
{
    public function index(){
        $compras = Compras::all();
        return view('compras.index', compact('compras'));
       
    }
    public function register($prod){
        $compras = new Compras();
        $compras->id_user = Auth::id();
        $compras->id_producto = $prod;
        $compras->save();
        return redirect()->route('productos.index');
    }
}
