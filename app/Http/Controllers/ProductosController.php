<?php

namespace App\Http\Controllers;

use App\Models\Productos;
use Illuminate\Http\Request;

class ProductosController extends Controller
{
    public function index(){
        $productos = Productos::paginate(10)->where('status', '=', '1');
        return view('productos.index', compact('productos'));
    }

    public function create(){
        return view('productos.create');
    }

    public function register(Request $request){
        $productos = new productos();
        $productos->descripcion = $request->descripcion;
        $productos->impuesto = $request->impuesto;
        $productos->precio_impuesto = $request->price_imp;
        $productos->save();
        return redirect()->route('productos.index');
    }

    public function edit($prod){
        $productos = Productos::where('id', '=', $prod)->get()->first();
        return view('productos.edit', compact('productos'));
    }

    public function actualizar(Request $request, productos $prod){
        $prod->descripcion = $request->descripcion;
        $prod->impuesto = $request->impuesto;
        $prod->precio_impuesto = ($request->price_imp);
        $prod->save();
        return redirect()->route('productos.index');
    }

    public function delete($prod){
        $prodToDelete = Productos::where('id', '=', $prod)->get()->first();
        $prodToDelete->status = '0';
        $prodToDelete->save();
        return redirect()->route('productos.index');
    }
}
