<?php

namespace App\Http\Controllers;

use App\Models\Compras;
use App\Models\Facturas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FacturasController extends Controller
{
    public function index(){
        $facturas = Facturas::all();
        return view('facturas.index', compact('facturas'));
    }

    public function indexPendiente(){
        $comprasFact = Compras::where('status', '=', '1');
        return view('facturas.index_pendientes', compact('comprasFact'));
    }

    public function facturarOne($comp){
        $compras = Compras::Where('id','=', $comp)->get()->first();
        $facturas = new Facturas();
        $facturas->id_compra = $comp;
        $facturas->costo_total = $compras->producto->precio_impuesto;
        $facturas->impuesto_facturado = ($compras->producto->precio_impuesto * ($compras->producto->impuesto) / 100);
        $compras->status = '0';
        $facturas->save();
        $compras->save();
        return redirect()->route('compras.index');

    }

}
