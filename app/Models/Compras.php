<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Compras extends Model
{
    use HasFactory;
    protected $table = "compras";

    public function user()
    {
        return $this->belongsTo(User::class, 'id_user');
    }

    public function producto()
    {
        return $this->belongsTo(Productos::class, 'id_producto');
    }

    public function factura() {
        return $this->hasMany(Factura::class, 'id_compra');        
    }

   
}
