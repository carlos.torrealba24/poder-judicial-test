<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Productos extends Model
{
    use HasFactory;
    protected $table = "productos";

    public function compras() {
        return $this->hasMany(Compras::class, 'id_producto');        
    }
}
