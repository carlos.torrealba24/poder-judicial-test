@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row mb-5">
            <h1>Compras</h1>
        </div>
        
        <div class="col-md-12">
            <table class="table table-light">
                <thead class="thead-light">
                    <tr>
                        <th class="text-center">ID</th>
                        <th class="text-center">Cliente</th>
                        <th class="text-center">Producto</th>
                        <th class="text-center">Estatus</th>
                        @role('administrador')
                            <th class="text-center">Action</th>
                        @endrole
                    </tr>
                </thead>
                <tbody>
                    
                    @foreach ($compras as $compra)
                    <tr>
                        <td class="text-center">{{$compra->id}}</td>
                        <td class="text-center">{{$compra->user->name}}</td>
                        <td class="text-center">{{$compra->producto->descripcion}}</td>
                        <td class="text-center">{{$compra->status == '1' ? 'Pendiente' : 'Facturado'}}</td>
                        @role('administrador')
                            <td class="text-center">
                                <div class="btn-group">
                                    @if ($compra->status == '1')
                                        <a href="{{ route('facturas.factura_one', $compra->id) }}"
                                            class="btn btn-success" title="Edit">Facturar
                                        </a>
                                    @endif()
                                </div>
                            </td>
                        @endrole
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
