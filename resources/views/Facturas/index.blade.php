@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row mb-5">
            <h1>Facturas</h1>
        </div>
        
        <div class="col-md-12">
            <table class="table table-light">
                <thead class="thead-light">
                    <tr>
                        <th class="text-center">ID</th>
                        <th class="text-center">Cliente</th>
                        <th class="text-center">Costo Total</th>
                        <th class="text-center">Impuesto Facturado</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($facturas as $fact)
                    <tr>
                        <td class="text-center">{{$fact->id}}</td>
                        <td class="text-center">{{$fact->compras->user->name}}</td>
                        <td class="text-center">{{$fact->costo_total}}</td>
                        <td class="text-center">{{$fact->impuesto_facturado}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
