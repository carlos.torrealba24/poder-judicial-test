@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="">
            <form action="{{route('productos.actualizar', $productos)}}" method="post">
                @csrf
                @method('PUT')
                <div class="form-group mb-3">
                    <div class="form-group mb-3">
                        <label for="">Descripcion:</label>
                        <input type="text" name="descripcion" id="descripcion" class="form-control" placeholder="escribe una descripcion" aria-describedby="helpId" value="{{$productos->descripcion}}">
                    </div>
                    <div class="form-group mb-3">
                        <label for="">Precio con Impuesto:</label>
                        <input type="text" name="price_imp" id="price_imp" class="form-control" placeholder="escribe un precio" aria-describedby="helpId" value="{{$productos->precio_impuesto}}">
                    </div>   
                    <div class="form-group mb-3">
                        <label for="">Impuesto:</label>
                        <input type="text" name="impuesto" id="impuesto" class="form-control" placeholder="escribe un impuesto" aria-describedby="helpId" value="{{$productos->impuesto}}">
                    </div>  
                </div>
                
                <div class="float-right">
                    <button class="btn btn-primary" type="submit">Editar</button>
                </div>
            </form>
        </div>
    </div>    
@endsection