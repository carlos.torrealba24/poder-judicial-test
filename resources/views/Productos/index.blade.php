@extends('layouts.app')

@section('content')
    <div class="container">
        @role('administrador')
            <div class="row mb-5">
                <div class="col-md-6 mb-5">
                    <a type="button" class ="btn btn-primary" href="{{route('productos.create')}}">Nuevo producto</a>
                </div>
            </div>
        @endrole
        
        <div class="col-md-12">
            <table class="table table-light">
                <thead class="thead-light">
                    <tr>
                        <th class="text-center">ID</th>
                        <th class="text-center">Description</th>
                        <th class="text-center">Precio</th>
                        <th class="text-center">Impuesto</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($productos as $prod)
                    <tr>
                        <td class="text-center">{{$prod->id}}</td>
                        <td class="text-center">{{$prod->descripcion}}</td>
                        <td class="text-center">{{$prod->precio_impuesto}}</td>
                        <td class="text-center">{{$prod->impuesto}}</td>
                        <td class="text-center">
                            <div class="btn-group">
                                <a href="{{ route('compras.register', $prod->id) }}"
                                    class="btn btn-success" title="Edit">Comprar
                                </a>
                                @role('administrador')
                                    <a href="{{ route('productos.edit', $prod->id) }}"
                                        class="btn btn-warning" title="Edit">Edit
                                    </a>
                                    <form action="{{url('productos/delete/'.$prod->id)}}" method="post">
                                        @method('DELETE')
                                        @csrf
                                        <button class="btn btn-danger" type="submit">Delete</button>
                                    </form>
                                @endrole
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
