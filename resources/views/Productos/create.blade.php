@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="">
            <form action="{{route('productos.register')}}" method="post">
                @csrf
                <div class="form-group mb-3">
                    <div class="form-group mb-3">
                        <label for="">Descripcion:</label>
                        <input type="text" name="descripcion" id="descripcion" class="form-control" placeholder="escribe una descripcion" aria-describedby="helpId">
                    </div>
                    <div class="form-group mb-3">
                        <label for="">Precio con Impuesto:</label>
                        <input type="text" name="price_imp" id="price_imp" class="form-control" placeholder="escribe un precio" aria-describedby="helpId">
                    </div>   
                    <div class="form-group mb-3">
                        <label for="">Impuesto:</label>
                        <input type="text" name="impuesto" id="impuesto" class="form-control" placeholder="escribe un impuesto" aria-describedby="helpId">
                    </div>  
                </div>
                
                <div class="float-right">
                    <button class="btn btn-success" type="submit">Registrar</button>
                </div>
            </form>
        </div>
    </div>    
@endsection
